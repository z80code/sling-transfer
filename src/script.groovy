def get = new URL("http://localhost:8080/conf/global.xml").openConnection();
get.setRequestProperty('user', 'admin')
get.setRequestProperty('password', 'admin')
def getRC = get.getResponseCode()
println(getRC)
if (getRC.equals(200)) {
    def xml = get.getInputStream().getText()
//      println xml
    def slurper = new XmlSlurper().parseText(xml)
    def map = convertToMap(slurper, '/home/z80code/sling/test')

    // println map.get('jcr:prymaryTipe')
}

def convertToMap(nodes, parent) {

    def attributes = reMapAttributes(nodes.attributes())
    def fileName = "${parent}/${nodes.name()}"
    println fileName
    def primaryType = attributes.get('jcr:primaryType')
    switch (primaryType) {
        case ["sling:OrderedFolder", 'sling:Folder']:
            createAttributeFile(fileName, attributes)
            break
        case '':
            break

    }

    def propsMap = new HashMap()
    def propsMapChildren = nodes.children().collectEntries {
        [it.name(), it.childNodes() ? convertToMap(it, fileName) : "'" + it.text() + "'"]
    }


    propsMap.put("attributes", attributes)

    // propsMap.put(nodes.name(), propsMapChildren)
    // add attributes
    // propsMap.put("children",

    //)
    return propsMap
}

def reMapAttributes(attributes) {
    return attributes.collectEntries {
        [
                it.key.contains('jcr') ? it.key.replace('{http://www.jcp.org/jcr/1.0}', 'jcr:') :
                        it.key.contains('sling') ? it.key.replace('{http://www.sling.apache.org/sling/1.0}', 'sling:') : it.key,
                it.value
        ]
    }
}

def createAttributeFile(fileName, attributes) {
    def sw = new StringWriter()
    def content = new groovy.xml.MarkupBuilder(sw)
    content.mkp.xmlDeclaration(version: "1.0", encoding: "utf-8")
    content.'jcr:root'(
            ['xmlns:jcr'  : 'http://www.jcp.org/jcr/1.0',
             'xmlns:sling': 'http://www.sling.apache.org/sling/1.0'
            ].plus(attributes)) {}

    new File("${fileName}").mkdirs()
    def f = new File("${fileName}/.content.xml")
    f.write(sw.toString())
}